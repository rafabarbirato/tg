import os
import cv2
import re

def get_values (hsv_p):

    avg_h_pattern = 0
    avg_s_pattern = 0
    avg_v_pattern = 0

    for i in range(hsv_p.shape[0]):
        for j in range(hsv_p.shape[1]):

            # Sum up of HSV pattern values
            avg_h_pattern += hsv_p.item(i, j, 0)
            avg_s_pattern += hsv_p.item(i, j, 1)
            avg_v_pattern += hsv_p.item(i, j, 2)

    # Average of HSV pattern values
    avg_h_pattern = avg_h_pattern / (hsv_p.shape[0] * hsv_p.shape[1])
    avg_s_pattern = avg_s_pattern / (hsv_p.shape[0] * hsv_p.shape[1])
    avg_v_pattern = avg_v_pattern / (hsv_p.shape[0] * hsv_p.shape[1])

    return (avg_h_pattern, avg_s_pattern, avg_v_pattern)


if __name__ == '__main__':

    list_pattern = os.listdir("c:/Users/Rafaela/Documents/Project/Patterns/")
    for item in list_pattern:

        arquivo = open(str(item) + '.txt', 'w')
        arquivo.write('@relation '+str(item)+ '\n@attribute 0 real \n@attribute 1 real \n@attribute 2 real ')


        if item == "Ascorbic_pattern":
            arquivo = open(str(item)+'.txt', 'r+')
            texto = arquivo.readlines()
            texto.append('\n@attribute class  {10, 25, 50, neg} \n \n@data')
            arquivo.writelines (texto)


            list_pattern = os.listdir("c:/Users/Rafaela/Documents/Project/Patterns/" + str(item))
            for pattern in list_pattern:

                path_pattern = ("c:/Users/Rafaela/Documents/Project/Patterns/" + str(item) + "/" + str(pattern))
                hsv_p = cv2.cvtColor(cv2.imread(path_pattern), cv2.COLOR_BGR2HSV)
                values = (get_values (hsv_p))
                class_value = re.sub(r" \(.*\).png", "", pattern)
                texto = arquivo.readlines()
                texto.append("{},{},{},{}".format(values[0], values[1], values[2], class_value))
                arquivo.writelines(texto)
                #arquivo.close()


        if item == "Bilirrubin_pattern":
            arquivo = open(str(item) + '.txt', 'r+')
            texto = arquivo.readlines()
            texto.append('\n@attribute class  {0.5, 1, neg} \n \n@data')
            arquivo.writelines (texto)

        if item == "Blood_pattern":
            arquivo = open(str(item) + '.txt', 'r+')
            texto = arquivo.readlines()
            texto.append('\n@attribute class  {5, 10, 50, 250, neg} \n \n@data')
            arquivo.writelines (texto)

        if item == "Cetones_pattern":
            arquivo = open(str(item) + '.txt', 'r+')
            texto = arquivo.readlines()
            texto.append('\n@attribute class  {5, 10, 50, 100, neg} \n \n@data')
            arquivo.writelines (texto)

        if item == "Density_pattern":
            arquivo = open(str(item) + '.txt', 'r+')
            texto = arquivo.readlines()
            texto.append('\n@attribute class  {1005, 1010, 1015, 1020, 1025, 1030} \n \n@data')
            arquivo.writelines (texto)

        if item == "Glucose_pattern":
            arquivo = open(str(item) + '.txt', 'r+')
            texto = arquivo.readlines()
            texto.append('\n@attribute class  {100, 250, 500, 2000, neg} \n \n@data')
            arquivo.writelines (texto)

        if item == "Leucocytes_pattern":
            arquivo = open(str(item) + '.txt', 'r+')
            texto = arquivo.readlines()
            texto.append('\n@attribute class  {10, 25, 75, 500, neg} \n \n@data')
            arquivo.writelines (texto)

        if item == "Nitrite_pattern":
            arquivo = open(str(item) + '.txt', 'r+')
            texto = arquivo.readlines()
            texto.append('\n@attribute class  {pos, neg} \n \n@data')
            arquivo.writelines (texto)

        if item == "pH_pattern":
            arquivo = open(str(item) + '.txt', 'r+')
            texto = arquivo.readlines()
            texto.append('\n@attribute class  {5, 5.5, 6, 6.5, 7, 7.5, 8, 8.5, neg} \n \n@data')
            arquivo.writelines (texto)

        if item == "Protein_pattern":
            arquivo = open(str(item) + '.txt', 'r+')
            texto = arquivo.readlines()
            texto.append('\n@attribute class  {10, 30, 100, 300, 1000, neg} \n \n@data')
            arquivo.writelines (texto)

        if item == "Urobilinogen_pattern":
            arquivo = open(str(item) + '.txt', 'r+')
            texto = arquivo.readlines()
            texto.append('\n@attribute class  {1, 4, 8, norm} \n \n@data')
            arquivo.writelines (texto)

        #texto = arquivo.readlines()
        #texto.append('Nova linha')
        #arquivo.writelines(texto)
        arquivo.close()